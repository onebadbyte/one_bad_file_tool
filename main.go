package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path"

	"github.com/pterm/pterm"
)

func main() {
	var path string
	var max int

	biggestFiles := make(map[string]int64)
	flag.StringVar(&path, "p", ".", "This path is where the tool will search from")
	flag.IntVar(&max, "m", 5, "Max files it will find")
	flag.Parse()
	pterm.Println(pterm.Cyan("Finding Biggest Files"))

	fileSystem := os.DirFS(".")
	rootDir, err := fs.ReadDir(fileSystem, path)
	if err != nil {
		panic(fmt.Sprintf("Couldn't find path: %v", err.Error()))
	}

	loopThroughDirRecursively(&rootDir, &biggestFiles, fileSystem, path, max)

	for key, value := range biggestFiles {
		printOutFiles(key, value)
	}

}

func printOutFiles(name string, size int64) {
	if size < 8000000 {
		fmt.Printf("%v : %vB \n", name, size)
	} else {
		fmt.Printf("%v : %vM \n", name, size/8000000)
	}
}

func loopThroughDirRecursively(dir *[]fs.DirEntry, biggestFiles *map[string]int64, fileSystem fs.FS, rootPath string, max int) {
	for _, entry := range *dir {
		info, err := entry.Info()
		if err != nil {
			fmt.Println(err)
		} else {
			AddToBiggestFile(biggestFiles, max, info, rootPath)
		}

		if info.IsDir() {
			newPath := path.Join(rootPath, info.Name())
			newDir, err := fs.ReadDir(fileSystem, newPath)
			if err != nil {
				panic(fmt.Sprintf("Couldn't find path :%v", err.Error()))
			}
			loopThroughDirRecursively(&newDir, biggestFiles, fileSystem, newPath, max)
		}
	}
}

func AddToBiggestFile(files *map[string]int64, maxSize int, info fs.FileInfo, basePath string) {

	if len(*files) <= maxSize && !info.IsDir() {
		(*files)[path.Join(basePath, info.Name())] = info.Size()
		return
	}

	for key, value := range *files {
		if value < info.Size() {
			delete(*files, key)
			(*files)[path.Join(basePath, info.Name())] = info.Size()
		}

	}

}
