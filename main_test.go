package main_test

import (
	"io/fs"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Describe("Main", func() {
	Context("Biggest Files", func() {

		It("should add biggest files", func() {

			biggestFiles := make(map[string]int64, 0)

			info := fs.FileInfo{
				Name: "stuff",
			}

			AddToBiggestFile(biggestFiles, 5, info)

			Expect()

		})
	})

})
